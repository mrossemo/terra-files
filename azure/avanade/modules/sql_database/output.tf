output "sql_database_id" {
  description = "ID do banco de dados SQL."
  value       = azurerm_mssql_database.sqlserver.id
}
